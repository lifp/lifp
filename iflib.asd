;; -*- mode:lisp -*-
;;ASDF system definition for loading if-lib and LIFP user interface
;;
;;This file is a part of Lisp Interactive Fiction Project
;;
;;See license.txt for licensing information


(use-package :asdf)

(defsystem iflib
    :name "iflib"
    :author "Timofei Shatrov <grue@mail.ru>"
    :description "Interactive Fiction Lisp library"
    :components
    ((:file "console")
     (:file "if" :depends-on ("console"))
     (:file "iflib" :depends-on ("if" "console"))
     (:file "verbs" :depends-on ("if" "iflib")))
    :depends-on (:ltk :cl-fad))
     