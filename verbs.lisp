;;Common Lisp Interactive Fiction Library 
;;
;;verb-lib module: defines verbs and their associated actions
;;
;;This file is a part of Lisp Interactive Fiction Project
;;
;;See license.txt for licensing information



(in-package :cl-user)

(defpackage :verb-lib
  (:use :common-lisp :if-lib :if-basic-lib)
  (:export :attack :take :teleport :examine 
	   :go-to :pass
	   :take :put-in :put-on :drop :receive
	   :wear :strip :enter :climb :drink :eat
           :rub :turn :switch-on :switch-off
           :fill :empty :extract :let-go :open :close
           :lock :unlock :unlock-open)
  (:shadow :listen :fill :open :close)
  (:shadowing-import-from :if-lib :room))

(in-package :verb-lib)

(defmacro const-fun (name args value)
  `(defun ,name ,args
    (declare (ignore ,@args))
    ,value))

(const-fun noargs-1 (c) nil)

(verb "quit" '(:meta -> quit-game noargs-1)) ;;That one you'll use often ;)

;Debug verb
(verb "teleport"
      `((:noun ,(lambda () *allobjects*)) -> teleport))

(verb "take"
      '(:noun -> take)
      '("off" :held -> strip)
      '(:held "off" -> strip)) 

(verb "get"
      '(:noun -> take))
   
(const-fun const-loc (c) *location*)

(verb "look" "l"
      `(-> look const-loc)
      '("at" :seen -> examine))

(verb "examine" "x"
      '(:noun -> examine))

(verb "attack" "break" "crack" "destroy"
     "fight" "hit" "kill" "murder" "punch"
     "smash" "thump" "torture" "wreck"
    '(:noun -> attack))

;(defmacro const-fun* (name args value)
;  `(defun ,name ,args
;    (declare (ignore ,@args))
;    (list *location* ,value)))

(const-fun cdir-n (c) dir-n)
(const-fun cdir-ne (c) dir-ne)
(const-fun cdir-e (c) dir-e)
(const-fun cdir-se (c) dir-se)
(const-fun cdir-s (c) dir-s)
(const-fun cdir-sw (c) dir-sw)
(const-fun cdir-w (c) dir-w)
(const-fun cdir-nw (c) dir-nw)
(const-fun cdir-u (c) dir-u)
(const-fun cdir-d (c) dir-d)
(const-fun cdir-in (c) dir-in)
(const-fun cdir-out (c) dir-out)

(verb "go" "run" "walk" 
      '(:direction -> go-to)
      '(:noun -> enter)
      '((:or "into" "in" "inside" "through") :noun -> enter rest))

(verb "n" "north" '(-> go-to cdir-n))
(verb "ne" "northeast" '(-> go-to cdir-ne))
(verb "e" "east" '(-> go-to cdir-e))
(verb "se" "southeast" '(-> go-to cdir-se))
(verb "s" "south" '(-> go-to cdir-s))
(verb "sw" "southwest" '(-> go-to cdir-sw))
(verb "w" "west" '(-> go-to cdir-w))
(verb "nw" "northwest" '(-> go-to cdir-nw))
(verb "u" "up" '(-> go-to cdir-u))
(verb "d" "down" '(-> go-to cdir-d))
(verb "in" '(-> go-to cdir-in))
(verb "out" '(-> go-to cdir-out))

(verb "enter" 
      '(:direction -> go-to)
      '(:noun -> enter))

(verb "inventory" "i" '(-> inventory))

(verb "take"
      '(:noun -> take)
      '("off" :held -> strip)
      '(:held "off" -> strip)
      '(:noun "from" :noun -> extract)
      '(:noun "from" :noun -> extract))

(verb "get"
      '(:noun -> take)
      '((:or "out" "off" "up") -> go-to cdir-out)
      '((:or "in" "into" "on" "onto") :noun -> enter rest)
      '(:noun "from" :noun -> extract))

(verb "drop" "discard" "throw"
      '(:held -> drop)
      '(:held "in" :noun -> put-in)
      '(:held "on" :noun -> put-on))
      
(verb "put"
      '(:held "on" :noun -> put-on)
      '(:held "in" :noun -> put-in)
      '(:held "down" -> drop)
      '("on" :held -> wear)
      '(:held -> drop))

(verb "wear" "don"
      '(:held -> wear))

(verb "remove"
      '(:held -> strip)
      '(:noun -> take)
      '(:noun "from" :noun -> extract))

(verb "shed" "disrobe" "doff"
      '(:held -> strip))

(verb "sit" "lie"
      '("on" "top" "of" :noun -> enter)
      '((:or "on" "in" "inside") :noun -> enter rest))

(verb "climb" "scale"
      '(:noun -> climb)
      '((:or "up" "over") :noun -> climb))

(verb "listen" "hear"
      '(-> listen const-loc)
      '(:noun -> listen)
      '("to" :noun -> listen))

(verb "drink" "sip" "swallow" '(:noun -> drink))

(verb "eat" "consume" '(:held -> eat))

(verb "rub" "clean" "dust" "polish" "scrub" "shine"
      "sweep" "wipe" '(:noun -> rub))

(verb "switch"
      '(:noun -> switch-on)
      '(:noun "on" -> switch-on)
      '(:noun "off" -> switch-off)
      '("on" :noun -> switch-on)
      '("off" :noun -> switch-off))

(verb "turn"
      '(:noun -> turn)
      '(:noun "on" -> switch-on)
      '(:noun "off" -> switch-off)
      '("on" :noun -> switch-on)
      '("off" :noun -> switch-off))

(verb "fill" '(:noun -> fill))
      
(verb "empty" '(:noun -> empty))

(verb "open" 
      '(:noun -> open)
      '(:noun "with" :held -> unlock-open))

(verb "close" '(:noun -> close))
(verb "shut" 
      '(:noun -> close)
      '("off" :noun -> switch-off)
      '(:noun "off" -> switch-off))

(verb "lock"
      '(:noun "with" :held -> lock))
(verb "unlock"
      '(:noun "with" :held -> unlock))
      

(defaction attack (obj) "Violence is not the answer.")

(defaction teleport (obj) 
  (go-to-room obj))

(defaction examine (obj)
  (if (provides obj 'description)
      (read-property obj 'description)
      (format nil "You see nothing special about ~A.~%" (the-name obj))))  

;;(defun look-around () (run-action 'look *location*))

(defaction go-to (dir)
  (let ((destination (read-property *location* (property dir))))
    (if destination (exec go-to-dispatch (destination) :str t)
	(if (provides *location* 'cant-go) 
	    (read-property *location* 'cant-go)
	    "You can't go here."))))

(defgeneric go-to-dispatch (dest)
  (:documentation "Dispatches between different kinds of goable objects"))

(defmethod go-to-dispatch ((dest room))
  (go-to-room dest))

(defmethod go-to-dispatch ((dest door))
  ;(format t "go-to-dispatch: ~a~%" dest)
  (unless (has dest :door) (return-from go-to-dispatch (call-next-method)))
  (if (has dest :closed) (format nil "~a is closed." (the-name dest :capital t))
      (run-action 'pass (list dest))))

(defaction pass (obj)
  "Something's wrong happened.")

(defmethod pass ((obj door))
  (go-to-dispatch (read-property obj 'destination))
  (run-action-after obj))

(defun inventory ()
  (sprint "You are carrying: ~a." (list-contents *player*))
  (newline))

(defaction take (obj)
  "You can't take that.")

(defmethod take ((obj item))
  (if (has obj :item)
      (if (in obj *player*) 
	  (progn (sprint "You already have ~A" (the-name obj)) t) 
	  (if (below obj (parent *player*))
              (let ((loc (parent *player*)))
                (and
                 (loop for x = (parent obj)
                    until (eql x loc)
                    always (run-action 'extract-silent (list obj x))
                    finally (return t))
                 (move obj *player*)
                 (run-action-after obj)
                 "Taken."))
              (sprint "You cannot take ~a from here." (the-name obj))))
      (call-next-method)))

(defaction drop (obj)
  (unless (has obj :item) (return-from drop "You can't drop that."))
  (when (has obj :worn)
    (sprint "(first removing ~a)~%" (the-name obj))
    (unless (run-action 'strip obj)
      (return-from drop "You can't drop it.")))  
  (move obj (parent *player*))
  (when (run-action-after obj) "Dropped."))

(defaction put-on (item host)
  "You can't put anything on that.")

(defmethod put-on ((item item) (host supporter))
  ;;(format t "(~a ~a)" (print-name item) (print-name host)) 
  (unless (has item :item) (return-from put-on "You can't get rid of that."))
  (unless (has host :supporter) (return-from put-on (call-next-method)))
  (when (has item :worn)
    (sprint "(first removing ~a)~%" (the-name item))
    (unless (run-action 'strip item)
      (return-from put-on "You can't drop it.")))
  (and (run-action 'receive (reverse *args*) :time 0)
       *after*
       (run-action-after item) 
       "Done."))

(defaction put-in (item host)
  "You can't put anything in that.")

(defmethod put-in ((item item) (host container))
  (unless (has item :item) (return-from put-in "You can't get rid of that."))
  (unless (has host :container) (return-from put-in (call-next-method)))
  (when (has host :closed) 
    (return-from put-in 
      (format nil "~a is closed." (the-name host :capital t))))
  (when (has item :worn)
    (sprint "(first removing ~a)~%" (the-name item))
    (unless (run-action 'strip item)
      (return-from put-in "You can't drop it.")))
  (and (run-action 'receive (reverse *args*) :time 0)
       *after*
       (run-action-after item) 
       "Done."))
    
(defaction receive (host guest)
  "No method defined for that kind of object movement.")

(defmethod receive ((host supporter) (item item))
  (if (or (zerop (capacity host)) 
	  (< (list-length (children host)) (capacity host)))
    (progn (move item host)
	   (run-action-after host))
    "Not enough space."))

(defmethod receive ((host container) (item item))
  (if (or (zerop (capacity host)) 
	  (< (list-length (children host)) (capacity host)))
    (progn (move item host)
	   (run-action-after host))
    "Not enough space."))

(defaction wear (what)
  "You can't wear that.")

(defmethod wear ((obj clothing))
  (if (has obj :clothing)
      (if (hasnt obj :worn) 
	  (progn 
	    (give obj :worn) (when (run-action-after obj) "Done."))
	  "You are already wearing it.")
      "You can't wear that."))

(defaction strip (what)
  "That's one strange thing you want to do.")

(defmethod strip ((obj clothing))
  (if (and (has obj :clothing) (has obj :worn))
      (progn (give obj :~worn) (when (run-action-after obj) "Done."))
      "You can't do that."))

(defaction enter (what)
  "You can't enter that.")

(defmethod enter ((door door))
  (go-to-dispatch door))

(defaction climb (what)
  "You can't climb that.")

(defaction listen (what)
  "You hear nothing unexpected.")

(defaction drink (what)
  "You can't drink that.")

(defaction eat (what)
  "You can't eat that.")

(defmethod eat ((obj food))
  (if (has obj :edible)
      (progn 
        (rmv obj)
        (when (run-action-after obj)
          (format nil "You eat ~a." (the-name obj))))
      (call-next-method)))

(defaction rub (what)
  "This action achieves nothing.")

(defaction turn (what)
  "That's fixed in place.")

(defmethod turn ((item item))
  (if (has item :item)
      "This action achieves nothing."
      (call-next-method)))

(defaction switch-on (what)
  "You can't switch this on")

(defaction switch-off (what)
  "You can't switch this off")

(defmethod switch-on ((obj switchable))
  (if (has obj :switchable)
      (progn
        (if (has obj :on)
            (format nil "~a is already on." (the-name obj :capital t))
            (progn (give obj :on)
                   (when (run-action-after obj) "Done."))))
      (call-next-method)))

(defmethod switch-off ((obj switchable))
  (if (has obj :switchable)
      (progn
        (if (hasnt obj :on)
            (format nil "~a is already off." (the-name obj :capital t))
            (progn (give obj :~on)
                   (when (run-action-after obj) "Done."))))
      (call-next-method)))        
                     
(defaction fill (what) "You can't fill that.")

(defaction empty (what) "That doesn't make sense.")

(defmethod empty ((obj container))
  (unless (has obj :container) (return-from empty (call-next-method)))
  (if (has obj :closed)
      "But it is closed!"
      (if (children obj)
          (objectloop (in x obj)
                      (sprint "~a: " (print-name x))
                      (run-action 'extract (list x obj)))
          "It is already empty.")))

(defaction extract (obj1 obj2 &key silent) "You can't do that.")

(defmethod extract ((item item) (host container) &key silent)
  (unless (has item :item) (return-from extract (call-next-method)))
  (unless (has host :container) (return-from extract (call-next-method)))
  (when (has host :closed) 
    (return-from extract 
      (format nil "~a is closed." (the-name host :capital t))))
  (and (run-action 'let-go (reverse *args*))
       *after*
       (run-action-after item) 
       (if silent t "Done.")))  

(defmethod extract ((item item) (host supporter) &key silent)
  (unless (has item :item) (return-from extract (call-next-method)))
  (unless (has host :supporter) (return-from extract (call-next-method)))
  (and (run-action 'let-go (reverse *args*))
       *after*
       (run-action-after item)
       (if silent t "Done.")))

(defaction extract-silent (obj1 obj2)
  (extract obj1 obj2 :silent t))

(defaction let-go (host thing)
  "Something's wrong happened.")

(defmethod let-go ((host supporter) (item item))
  (move item (parent host))
  (run-action-after host))

(defmethod let-go ((host container) (item item))
  (move item (parent host))
  (run-action-after host))

(defaction open (obj)
  "You cannot open this.")

(defmethod open ((obj predoor))
  (unless (and (or (has obj :container) (has obj :door)) (has obj :openable))
    (return-from open (call-next-method)))  
  (if (has obj :closed)
      (if (hasnt obj :locked)
          (progn 
            (give obj :~closed)
            (when (run-action-after obj)
              (format nil "You open ~a." (the-name obj))))
          "It's locked.")
      (format nil "~a is already open." (the-name obj :capital t))))

(defaction close (obj)
  "You cannot close this.")

(defmethod close ((obj predoor))
  (unless (and (or (has obj :container) (has obj :door)) (has obj :openable))
    (return-from close (call-next-method)))
  (if (hasnt obj :closed)
      (progn 
        (give obj :closed)
        (when (run-action-after obj)
          (format nil "You close ~a." (the-name obj))))
      (format nil "~a is already closed." (the-name obj :capital t))))

(defaction lock (obj key)
  "Not lockable.")

(defmethod lock ((obj predoor) (key item))
  (unless (and (or (has obj :container) (has obj :door)) 
               (has obj :openable)
               (has obj :lockable))
    (return-from lock (call-next-method)))
  (if (has obj :locked) 
      (format nil "~a is already locked." (the-name obj :capital t))
      (if (hasnt obj :closed)
          (format nil "~a is not closed." (the-name obj :capital t))
          (if (with-keys obj key)
              (progn
                (give obj :locked)
                (when (run-action-after obj)
                  (format nil "You lock ~a." (the-name obj))))
              (format nil "You cannot lock ~a with ~a."
                      (the-name obj) (the-name key))))))

(defaction unlock (obj key)
  "There is nothing to unlock.")

(defmethod unlock ((obj predoor) (key item))
  (unless (and (or (has obj :container) (has obj :door)) 
               (has obj :openable)
               (has obj :lockable))
    (return-from unlock (call-next-method)))
  (if (hasnt obj :locked) 
      (format nil "~a is already unlocked." (the-name obj :capital t))
      (if (hasnt obj :closed)
          (format nil "~a is not closed." (the-name obj :capital t))
          (if (with-keys obj key)
              (progn
                (give obj :~locked)
                (when (run-action-after obj)
                  (format nil "You unlock ~a." (the-name obj))))
              (format nil "You cannot unlock ~a with ~a."
                      (the-name obj) (the-name key))))))

(defaction unlock-open (obj key)
  "You cannot open this.")

(defmethod unlock-open ((obj predoor) (key item))
  (unless (and (or (has obj :container) (has obj :door)) 
               (has obj :openable))
    (return-from unlock-open (call-next-method)))
  (and (run-action 'unlock *args*)
       (run-action 'open obj)))
