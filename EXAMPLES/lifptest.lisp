(if-lib::load-libs :lifp-test)

(in-package :lifp-test)

(object bigroom (room) "The Big Room"
        (description "This is the Big Room. It's main purpose is
        to host the devices that are used to test various
        features of LIFP. The door to the north leads to closet.")
        (n-to 'closetdoor))

(object bigkey (item) "big key" bigroom
        (description "This is a big key. It is probably used to
        open something big"))

(object bigcrate (container) "big crate" bigroom
        (name "big" "box" "crate" "keyhole")
        (description "This big crate is a large container which can be closed or opened. It also has a keyhole.")
        (with-keys bigkey)
        (has :openable :closed :lockable :locked))

(object closetdoor (door) "door to closet" bigroom
        (name "door")
        (description "The door that leads to closet")
        (destination 'closet))

(object closet (room) "Small Closet"
        (description "This closet is small and dimly lit.")
        (s-to 'bigroom))

(supply init ()
   (setf *location* bigroom)
   "~%~%Somehow you ended up in some big room. But hey, what do
you expect in a cheap demo game?~%~%")
        