(if-lib::load-libs :heidi)

(in-package :heidi)

(object before-cottage (room) "In front of a cottage"
	(description "You stand outside a cottage. The forest stretches east.")
	(e-to 'forest)
	(in-to "It's such a lovely day -- much too nice to go inside.")
	(cant-go "The only path lies to the east."))

(object cottage (scenery) "tiny cottage" before-cottage
	(description "It's small and simple, but you're very happy here.")
	(name "tiny" "cottage" "home" "house" "hut" "shed" "hovel")
	(before
	 (enter "It's such a lovely day -- much too nice to go inside.")))

(object forest (room) "Deep in the forest"
	(description "Through the dense foliage, you glimpse a
             building to the west. A track heads to the northeast.")
	(w-to 'before-cottage)
	(ne-to 'clearing))

(object bird (item) "baby bird" forest
	(description "Too young to fly, the nestling tweets helplessly.")
	(name "baby" "bird" "nestling")
	(before 
	 (listen "It sounds scared and in need of assistance.")))

(object clearing (room) "A forest clearing"
	(description "A tall sycamore stands in the middle of this clearing.
             The path winds southwest through the trees.")
	(sw-to 'forest)
	(u-to 'top-of-tree))

(object nest (item container) "bird's nest" clearing
	(description "The nest is carefully woven of twigs and moss.")
	(name "bird's" "nest" "twigs" "moss")
	(has :open))

(ref top-of-tree)

(object tree (scenery) "tall sycamore tree" clearing
	(description "Standing proud in the middle of the clearing,
             the stout tree looks easy to climb.")
	(name "tall" "sycamore" "tree" "stout" "proud")
	(before
	 (climb (go-to-room top-of-tree) t)))

(object top-of-tree (room) "At the top of the tree"
	(description "You cling precariously to the trunk.")
	(d-to 'clearing)
	(after
	 (drop (move *noun* clearing) nil)))

(object branch (supporter) "wide firm bough" top-of-tree
	(description "It's flat enough to support a small object.")
	(name "wide" "firm" "flat" "bough" "branch")
	(each-turn (lambda ()
		     (when (and (in bird nest) (in nest branch))
		       (setf *gamestate* 2))))
	(has :static))

(supply init ()
	(setf *location* before-cottage))



				

	
		     
	  