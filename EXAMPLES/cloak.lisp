(if-lib::load-libs :cloak-of-darkness)

(in-package :cloak-of-darkness)

(ref cloak message)
                                        
(object foyer (room) "Foyer of the Opera House"
	(description "You are standing in a spacious hall, splendidly
 decorated in red and gold, with glittering chandeliers overhead.
 The entrance from the street is to the north, and there are doorways
 south and west.")
	(s-to 'bar)
	(w-to 'cloakroom)
	(n-to "You've only just arrived, and besides, the weather outside
 seems to be getting worse."))

(object cloakroom (room) "Cloakroom"
	(description "The walls of this small room were clearly once lined
 with hooks, though now only one remains. The exit is a door to the east.")
	(e-to 'foyer))

(object hook (supporter) "small brass hook" cloakroom
	(name "small" "brass" "hook" "peg")
	(description (lambda () (format nil "It's just a small brass hook, ~a"
					(if (in cloak *player*)
					    "screwed to the wall."
					    "with a cloak hanging on it."))))
	(has :scenery))

(object bar (room) "Foyer bar"
	(description "The bar, much rougher than you'd have guessed after
 the opulence of the foyer to the north, is completely empty. There seems
 to be some sort of message scrawled in the sawdust on the floor.")
	(n-to 'foyer)
	(before
	 (go-to (when (and (hasnt self :light) 
			   (not (eql *noun* dir-n)))
		  (incf (num message) 2)
		  "Blundering around in the dark isn't a good idea!"))
	 (t (when (hasnt self :light)
	      (incf (num message) 1)
	      "In the dark? You could easily disturb something!")))
	(has :~light))

(object cloak (clothing) "velvet cloak" *player*
	(name "handsome" "dark" "black" "velvet" "satin" "cloak")
	(description "A handsome cloak, of velvet trimmed with satin, and
 slightly spattered with raindrops. Its blackness is so deep that it almost
 seems to suck light from the room.")
	(before
	 ((drop put-on)
	  (if (eql *location* cloakroom)
	    (progn (give bar :light)
		   (when (and (eql *action* 'put-on) (has self :general))
		     (give self :~general)
		     (incf *score*) nil))
	    "This isn't the best place to leave a smart cloak lying around.")))
	(after (take (give bar :~light) nil))
	(has :general :worn))

(object message () "scrawled message" bar
	(name "message" "sawdust" "floor")
	(description (lambda ()
		       (if (< (num message) 2)
			   (progn (incf *score*) 
				  (setf *gamestate* 2)
				  (sprint "The message, neatly marked in the
 sawdust, reads..."))
			   (progn (setf *gamestate* 3)
				  (sprint "The message has been carelessly
 trampled, making it difficult to read. You can just distinguish
 the words...")))))
	(num integer 0)
	(has :scenery))
				  
(supply init ()
  (setf *location* foyer)
  "~%~%Hurrying through the rainswept November night, you're glad to see
 the bright lights of the Opera House. It's surprising that there aren't
 more people about but, hey, what do you expect in a cheap demo game...?~%~%")

(supply print-gamestate () "You have lost")

(verb "hang" '(:held "on" :noun -> put-on))